/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat_system;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import static java.awt.SystemColor.desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.filechooser.FileNameExtensionFilter;


/**
 *
 * @author Yash
 */
public class Chat_system extends javax.swing.JFrame implements MouseListener {

//    JPanel p1;
    String msg = "";
    String sender = "";
    String reciever = "";
    String curr_n = "";
    
    javax.swing.JLabel[] contact_l;
    javax.swing.JPanel[] p1;
    int contact_l_size = 0;
    
    int msg_loc = 10;
    int contact_p_count = 0;
    Chat_system ui = this;
    
    File sourceFile = null;
    
    int attach_flag = 0;
    attach_panel ap;
    
    JSONParser jsonparser = new JSONParser(ui);
    
    Connection conn = null;
    ResultSet rs = null;
    PreparedStatement ps = null;
    
     public static final Color blue_shade = new Color(55,65,83,255);
     public static final Color light_blue = new Color(51,106,190,255);
     public static final Color msg_back = new Color(220,248,198,255);
     public static final Color contact_back = new Color(36,46,66,255);
    public Chat_system() {
        initComponents();
        p_back.setVisible(false);
        p_login_back.setVisible(true);
        p_contact1.setVisible(false);
        p_contact2.setVisible(false);
        p_contact3.setVisible(false);
        
        conn = MysqlConnect.connectDB();
        
        getCountOfData();
        
    }

    String get_current_name(){
        curr_n = lbl_current_name.getText();
        return curr_n;
    }
    void getCountOfData(){
        try{
            String sql = "SELECT * FROM contacts";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next())
                contact_l_size++;
        }catch(Exception e){
            JOptionPane.showMessageDialog(this,"Exception in getCountData: "+e);
        }
    }
    void fill_contacts(){
        String name = "";
        int j = 0;
        int c_i = 1;
        try{
            String sql = "SELECT * FROM contacts";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
//            JOptionPane.showMessageDialog(this,"data count: "+contact_l_size);
            contact_l = new JLabel[contact_l_size];
            p1 = new JPanel[contact_l_size];
            while(rs.next()){
                
                name = rs.getString("name");
//                JOptionPane.showMessageDialog(this,"dbname:"+name);
//                JOptionPane.showMessageDialog(this,"currname:"+lbl_current_name.getText());
                if(!(name.equals((lbl_current_name.getText()).trim()))){
                    p1[contact_p_count] = new JPanel();
//                    JLabel l1 = new JLabel(name);
                    contact_l[contact_p_count] = new JLabel(name); 
//                    contact_l[contact_p_count].setAlignmentY(JLabel.CENTER_ALIGNMENT);
                    p1[contact_p_count].add(contact_l[contact_p_count]);
                    contact_l[contact_p_count].addMouseListener(new MouseAdapter(){
                        @Override
                        public void mouseClicked(MouseEvent me){
                            for(int i=0;i<contact_p_count;i++){
                                if(me.getSource() == contact_l[i]){
                                    String rec = contact_l[i].getText();
                                    setReciever(rec);
                                    lbl_current_reciever.setText(rec);
                                    try {
                                        jsonparser.readJSON(lbl_current_name.getText(), rec, ui);
                                    } catch (IOException ex) {
                                        Logger.getLogger(Chat_system.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }    
                            }  
                        }
                        public void mouseEntered(MouseEvent me){
                            for(int i=0;i<contact_p_count;i++){
                                if(me.getSource() == contact_l[i]){
                                    contact_l[i].setBackground(blue_shade);
                                    p1[i].setBackground(blue_shade);
                                }  
                            }   
                        }
                        public void mouseExited(MouseEvent me){
                            for(int i=0;i<contact_p_count;i++){
                                if(me.getSource() == contact_l[i]){
                                    contact_l[i].setBackground(contact_back);
                                    p1[i].setBackground(contact_back);
                                }  
                            } 
                        }
                    });
                   
                    contact_l[contact_p_count].setForeground(Color.WHITE);
                    p1[contact_p_count].setBounds(0,j,382,90);
                    contact_l[contact_p_count].setFont(new Font("Raleway", Font.PLAIN, 16));
                    p1[contact_p_count].setBackground(contact_back);
                    contact_l[contact_p_count].setBackground(contact_back);
                    p_contacts.add(p1[contact_p_count]);

                    j += 90;
                    contact_p_count++;
                    repaint();
                    p_contacts.updateUI();
                }
            }
        }catch(Exception e){
            System.out.println("Exception in Fill Contacts");
        }
    }
    
//    void add_msg(String msg){
//        JLabel lbl_m = new JLabel();
//        lbl_m.setBounds(100,msg_loc,300,50);
//        msg_loc += 60;
//        lbl_m.setText(msg);
//        p_middle.add(lbl_m);
//        repaint();
    
    
//    }
    void add_msg(String msg, String align){
        JLabel lbl_m = null;
        System.out.println("ALIGN: "+align);
        
        //Selecting alignment(left or right)
//        JOptionPane.showMessageDialog(this,"addmsg f name: "+f.getName());

        if(align.equals("left"))
            lbl_m = new JLabel(msg, JLabel.LEFT);
        else if(align.equals("right"))
            lbl_m = new JLabel(msg, JLabel.RIGHT);
        
        //Cheking that it has file or not

        lbl_m.setBounds(100,msg_loc,500,50);
        msg_loc += 60;
        p_middle.add(lbl_m);
        repaint();
    }
    void add_file(String align, File f){
        
        //Selecting alignment(left or right)
        fileMsg_panel fmp = new fileMsg_panel(f.getName(),ui,f);
        if(align.equals("left"))
            fmp.setBounds(100,msg_loc,259,85);
        else if(align.equals("right"))
            fmp.setBounds(360,msg_loc,259,85);
        
        //Cheking that it has file or not

        fmp.getLabel().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if(f.exists()){
                    Desktop desktop = Desktop.getDesktop();
                    try {
                        desktop.open(f);
                    } catch (IOException ex) {
                        Logger.getLogger(Chat_system.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
        msg_loc += 112;
        p_middle.add(fmp);
        p_middle.updateUI();
        repaint();
    }
    void setMsg(){
         msg = tf_msg.getText();
    }
    String getMsg(){
        return msg;
    }
    void clearMsg(){
        msg = "";
    }
    void setReciever(String rec){
        reciever = rec;
    }
    String getReciever(){
        return reciever;
    }
    
    
    void downloadFile(File file){
        String fileName = file.getName();
        File newFile = new File("my downloads/"+fileName);
        FileInputStream instream = null;
	FileOutputStream outstream = null;
        try {
            instream = new FileInputStream(file);
    	    outstream = new FileOutputStream(newFile);
            byte[] buffer = new byte[1024];
            
            int length;
    	    
    	    while ((length = instream.read(buffer)) > 0){
    	    	outstream.write(buffer, 0, length);
    	    }
            
            instream.close();
    	    outstream.close();
            
        } catch (IOException ex) {
            Logger.getLogger(Chat_system.class.getName()).log(Level.SEVERE, null, ex);
        }catch (Exception e) {
            Logger.getLogger(Chat_system.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    public String authenticate(String name, String pass){
        String flag = "false";
        try{
            Socket auth_s = new Socket("localhost",6000);
            OutputStream out = auth_s.getOutputStream();
            BufferedReader input = new BufferedReader(new InputStreamReader(auth_s.getInputStream()));
            
            name = name + "\n";
            byte buff[]  = name.getBytes();
            out.write(buff);
            
            pass = pass + "\n";
            byte buff_p[]  = pass.getBytes();
            out.write(buff_p);
            
            flag = input.readLine();
            System.out.println("retured flag: "+flag);
            if(flag.equals("false")){
               auth_s.close(); 
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,"Error in authenticate function: "+e);
        }
        return flag;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        p_login_back = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel9 = new javax.swing.JLabel();
        tf_password = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        p_login = new javax.swing.JPanel();
        lbl_login = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        lbl_login_status = new javax.swing.JLabel();
        p_back = new javax.swing.JPanel();
        p_left = new javax.swing.JPanel();
        tf_search = new javax.swing.JTextField();
        lbl_current_name = new javax.swing.JLabel();
        p_contacts = new javax.swing.JPanel();
        p_contact1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lbl_name1 = new javax.swing.JLabel();
        p_contact2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        lbl_name2 = new javax.swing.JLabel();
        p_contact3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        lbl_name3 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        p_right = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        tf_msg = new javax.swing.JTextField();
        lbl_send = new javax.swing.JLabel();
        lbl_attach = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        p_middle = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        lbl_current_reciever = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        p_login_back.setBackground(new java.awt.Color(3, 16, 62));

        jPanel3.setBackground(new java.awt.Color(3, 16, 62));
        jPanel3.setPreferredSize(new java.awt.Dimension(646, 855));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/chat_system/login-transparent.png"))); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(0, 96, Short.MAX_VALUE)
                .addComponent(jLabel2))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(155, 155, 155)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 572, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(3, 16, 62));
        jPanel4.setForeground(new java.awt.Color(255, 255, 255));

        jLabel4.setFont(new java.awt.Font("Raleway SemiBold", 0, 36)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Welcome Back");

        jLabel6.setFont(new java.awt.Font("Raleway SemiBold", 0, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Login");

        jLabel8.setFont(new java.awt.Font("Raleway SemiBold", 0, 16)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(51, 106, 190));
        jLabel8.setText("Username");

        jTextField1.setBackground(new java.awt.Color(3, 16, 62));
        jTextField1.setForeground(new java.awt.Color(255, 255, 255));
        jTextField1.setBorder(null);
        jTextField1.setNextFocusableComponent(tf_password);

        jSeparator1.setBackground(new java.awt.Color(51, 106, 190));
        jSeparator1.setForeground(new java.awt.Color(51, 106, 190));

        jLabel9.setFont(new java.awt.Font("Raleway SemiBold", 0, 16)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(51, 106, 190));
        jLabel9.setText("Password");

        tf_password.setBackground(new java.awt.Color(3, 16, 62));
        tf_password.setForeground(new java.awt.Color(255, 255, 255));
        tf_password.setBorder(null);
        tf_password.setNextFocusableComponent(lbl_login);

        jSeparator2.setBackground(new java.awt.Color(51, 106, 190));
        jSeparator2.setForeground(new java.awt.Color(51, 106, 190));

        p_login.setBackground(new java.awt.Color(51, 106, 190));

        lbl_login.setFont(new java.awt.Font("Raleway SemiBold", 0, 18)); // NOI18N
        lbl_login.setForeground(new java.awt.Color(255, 255, 255));
        lbl_login.setText("                                     Get Started");
        lbl_login.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_login.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_loginMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbl_loginMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbl_loginMouseExited(evt);
            }
        });

        javax.swing.GroupLayout p_loginLayout = new javax.swing.GroupLayout(p_login);
        p_login.setLayout(p_loginLayout);
        p_loginLayout.setHorizontalGroup(
            p_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lbl_login, javax.swing.GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE)
        );
        p_loginLayout.setVerticalGroup(
            p_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lbl_login, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)
        );

        jLabel10.setFont(new java.awt.Font("Raleway SemiBold", 0, 13)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(115, 132, 169));
        jLabel10.setText("Welcome to our app");

        jLabel11.setFont(new java.awt.Font("Raleway SemiBold", 0, 13)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(115, 132, 169));
        jLabel11.setText("Please login to enjoy our messenger");

        lbl_login_status.setFont(new java.awt.Font("Raleway", 1, 18)); // NOI18N
        lbl_login_status.setForeground(new java.awt.Color(255, 0, 0));
        lbl_login_status.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(220, 220, 220)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_login_status, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jTextField1)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel8)
                        .addComponent(jSeparator1)
                        .addComponent(jLabel9)
                        .addComponent(tf_password)
                        .addComponent(jSeparator2)
                        .addComponent(p_login, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 219, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(162, 162, 162)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(58, 58, 58)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tf_password, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50)
                .addComponent(p_login, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lbl_login_status, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(175, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout p_login_backLayout = new javax.swing.GroupLayout(p_login_back);
        p_login_back.setLayout(p_login_backLayout);
        p_login_backLayout.setHorizontalGroup(
            p_login_backLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p_login_backLayout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        p_login_backLayout.setVerticalGroup(
            p_login_backLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 908, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        p_back.setBackground(new java.awt.Color(255, 255, 255));

        p_left.setBackground(new java.awt.Color(36, 46, 66));

        tf_search.setFont(new java.awt.Font("Raleway SemiBold", 0, 16)); // NOI18N
        tf_search.setForeground(new java.awt.Color(216, 222, 226));
        tf_search.setText("                                 Search");
        tf_search.setToolTipText("Search");
        tf_search.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        tf_search.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tf_searchFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tf_searchFocusLost(evt);
            }
        });
        tf_search.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tf_searchMouseClicked(evt);
            }
        });
        tf_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tf_searchActionPerformed(evt);
            }
        });

        lbl_current_name.setFont(new java.awt.Font("Raleway Medium", 1, 18)); // NOI18N
        lbl_current_name.setForeground(new java.awt.Color(255, 255, 255));
        lbl_current_name.setText("Yash Thakkar");

        p_contacts.setBackground(new java.awt.Color(36, 46, 66));
        p_contacts.setPreferredSize(new java.awt.Dimension(372, 626));

        p_contact1.setBackground(new java.awt.Color(36, 46, 66));
        p_contact1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p_contact1MouseClicked(evt);
            }
        });

        jLabel1.setBackground(new java.awt.Color(36, 46, 66));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/chat_system/profile_64.png"))); // NOI18N

        lbl_name1.setBackground(new java.awt.Color(36, 46, 66));
        lbl_name1.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        lbl_name1.setForeground(new java.awt.Color(255, 255, 255));
        lbl_name1.setText("  neo");
        lbl_name1.setToolTipText("");
        lbl_name1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_name1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_name1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout p_contact1Layout = new javax.swing.GroupLayout(p_contact1);
        p_contact1.setLayout(p_contact1Layout);
        p_contact1Layout.setHorizontalGroup(
            p_contact1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p_contact1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl_name1, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        p_contact1Layout.setVerticalGroup(
            p_contact1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, p_contact1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contact1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbl_name1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        p_contact2.setBackground(new java.awt.Color(36, 46, 66));

        jLabel3.setBackground(new java.awt.Color(36, 46, 66));
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/chat_system/profile_64.png"))); // NOI18N

        lbl_name2.setBackground(new java.awt.Color(36, 46, 66));
        lbl_name2.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        lbl_name2.setForeground(new java.awt.Color(255, 255, 255));
        lbl_name2.setText("jhon doe");
        lbl_name2.setToolTipText("");
        lbl_name2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_name2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_name2MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout p_contact2Layout = new javax.swing.GroupLayout(p_contact2);
        p_contact2.setLayout(p_contact2Layout);
        p_contact2Layout.setHorizontalGroup(
            p_contact2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p_contact2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl_name2, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        p_contact2Layout.setVerticalGroup(
            p_contact2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, p_contact2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contact2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbl_name2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        p_contact3.setBackground(new java.awt.Color(36, 46, 66));

        jLabel5.setBackground(new java.awt.Color(36, 46, 66));
        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/chat_system/profile_64.png"))); // NOI18N

        lbl_name3.setBackground(new java.awt.Color(36, 46, 66));
        lbl_name3.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        lbl_name3.setForeground(new java.awt.Color(255, 255, 255));
        lbl_name3.setText("  jack");
        lbl_name3.setToolTipText("");
        lbl_name3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_name3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_name3MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout p_contact3Layout = new javax.swing.GroupLayout(p_contact3);
        p_contact3.setLayout(p_contact3Layout);
        p_contact3Layout.setHorizontalGroup(
            p_contact3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p_contact3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl_name3, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        p_contact3Layout.setVerticalGroup(
            p_contact3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, p_contact3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contact3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lbl_name3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout p_contactsLayout = new javax.swing.GroupLayout(p_contacts);
        p_contacts.setLayout(p_contactsLayout);
        p_contactsLayout.setHorizontalGroup(
            p_contactsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(p_contact2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(p_contact3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(p_contact1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        p_contactsLayout.setVerticalGroup(
            p_contactsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p_contactsLayout.createSequentialGroup()
                .addComponent(p_contact1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(p_contact2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(p_contact3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jLabel7.setFont(new java.awt.Font("Raleway SemiBold", 0, 13)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(216, 222, 226));
        jLabel7.setText("Contacts");

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/chat_system/logout.png"))); // NOI18N
        jLabel12.setToolTipText("Logut");

        javax.swing.GroupLayout p_leftLayout = new javax.swing.GroupLayout(p_left);
        p_left.setLayout(p_leftLayout);
        p_leftLayout.setHorizontalGroup(
            p_leftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p_leftLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(p_leftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(p_leftLayout.createSequentialGroup()
                        .addComponent(lbl_current_name, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(tf_search, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(39, Short.MAX_VALUE))
            .addComponent(p_contacts, javax.swing.GroupLayout.DEFAULT_SIZE, 382, Short.MAX_VALUE)
        );
        p_leftLayout.setVerticalGroup(
            p_leftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p_leftLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(p_leftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lbl_current_name, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(33, 33, 33)
                .addComponent(tf_search, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(p_contacts, javax.swing.GroupLayout.DEFAULT_SIZE, 709, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        p_right.setBackground(new java.awt.Color(255, 255, 255));
        p_right.setPreferredSize(new java.awt.Dimension(372, 100));

        javax.swing.GroupLayout p_rightLayout = new javax.swing.GroupLayout(p_right);
        p_right.setLayout(p_rightLayout);
        p_rightLayout.setHorizontalGroup(
            p_rightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 340, Short.MAX_VALUE)
        );
        p_rightLayout.setVerticalGroup(
            p_rightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setLayout(null);

        tf_msg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tf_msgActionPerformed(evt);
            }
        });
        jPanel1.add(tf_msg);
        tf_msg.setBounds(60, 18, 560, 42);

        lbl_send.setIcon(new javax.swing.ImageIcon(getClass().getResource("/chat_system/send.png"))); // NOI18N
        lbl_send.setToolTipText("Send");
        lbl_send.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_send.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_sendMouseClicked(evt);
            }
        });
        jPanel1.add(lbl_send);
        lbl_send.setBounds(638, 13, 50, 47);

        lbl_attach.setIcon(new javax.swing.ImageIcon(getClass().getResource("/chat_system/attachment.png"))); // NOI18N
        lbl_attach.setToolTipText("Attach File");
        lbl_attach.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_attach.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_attachMouseClicked(evt);
            }
        });
        jPanel1.add(lbl_attach);
        lbl_attach.setBounds(695, 13, 51, 47);

        jScrollPane1.setBorder(null);
        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setAutoscrolls(true);

        p_middle.setBackground(new java.awt.Color(255, 255, 255));
        p_middle.setLayout(null);
        jScrollPane1.setViewportView(p_middle);

        lbl_current_reciever.setFont(new java.awt.Font("Raleway", 1, 16)); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(lbl_current_reciever, javax.swing.GroupLayout.PREFERRED_SIZE, 405, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(320, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_current_reciever, javax.swing.GroupLayout.DEFAULT_SIZE, 52, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout p_backLayout = new javax.swing.GroupLayout(p_back);
        p_back.setLayout(p_backLayout);
        p_backLayout.setHorizontalGroup(
            p_backLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p_backLayout.createSequentialGroup()
                .addComponent(p_left, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(p_backLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(p_backLayout.createSequentialGroup()
                        .addGroup(p_backLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(0, 0, 0)))
                .addComponent(p_right, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        p_backLayout.setVerticalGroup(
            p_backLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(p_left, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(p_right, javax.swing.GroupLayout.DEFAULT_SIZE, 908, Short.MAX_VALUE)
            .addGroup(p_backLayout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane1)
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(p_back, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(p_login_back, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(p_back, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(p_login_back, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void p_contact1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p_contact1MouseClicked
        // TODO add your handling code here:
        
    }//GEN-LAST:event_p_contact1MouseClicked

    private void lbl_name1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_name1MouseClicked
        // TODO add your handling code here:
        String rec = lbl_name1.getText();
        setReciever(rec);
        p_contact1.setBackground(blue_shade);
        lbl_name1.setBackground(blue_shade);
    }//GEN-LAST:event_lbl_name1MouseClicked

    private void tf_searchFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tf_searchFocusGained
        // TODO add your handling code here:
       // tf_search.setText("");
    }//GEN-LAST:event_tf_searchFocusGained

    private void tf_searchFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tf_searchFocusLost
        // TODO add your handling code here:
       // tf_search.setText("                                 Search");
    }//GEN-LAST:event_tf_searchFocusLost

    private void tf_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tf_searchActionPerformed
        // TODO add your handling code here:
       
    }//GEN-LAST:event_tf_searchActionPerformed

    private void tf_searchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tf_searchMouseClicked
        // TODO add your handling code here:
         tf_search.setText("");
    }//GEN-LAST:event_tf_searchMouseClicked

    private void lbl_name2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_name2MouseClicked
        // TODO add your handling code here:
        String rec = lbl_name2.getText();
        setReciever(rec);
        p_contact2.setBackground(blue_shade);
        lbl_name2.setBackground(blue_shade);
    }//GEN-LAST:event_lbl_name2MouseClicked

    private void lbl_name3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_name3MouseClicked
        // TODO add your handling code here:
        String rec = lbl_name3.getText();
        setReciever(rec);
        p_contact3.setBackground(blue_shade);
        lbl_name3.setBackground(blue_shade);
    }//GEN-LAST:event_lbl_name3MouseClicked

    private void lbl_sendMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_sendMouseClicked
        // TODO add your handling code here: 
       setMsg();
       add_msg(msg,"right");
       tf_msg.setText(""); 
    }//GEN-LAST:event_lbl_sendMouseClicked

    private void lbl_loginMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_loginMouseClicked
        // TODO add your handling code here:
        
        sender = jTextField1.getText();
        String password = tf_password.getText();
        String there_is_client = authenticate(sender,password);
//        JOptionPane.showMessageDialog(null,"inclick event: "+there_is_client);
        if(there_is_client.equals("true")){
            there_is_client = "false";
            p_login_back.setVisible(false);
            p_back.setVisible(true);
            lbl_current_name.setText(sender);
            fill_contacts();
            new MyClient(sender, ui);
        }
        else{
            lbl_login_status.setText("Invalid credentials !");
        }
    }//GEN-LAST:event_lbl_loginMouseClicked

    private void lbl_loginMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_loginMouseEntered
        // TODO add your handling code here:
        p_login.setBackground(Color.white);
        lbl_login.setForeground(blue_shade);
    }//GEN-LAST:event_lbl_loginMouseEntered

    private void lbl_loginMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_loginMouseExited
        // TODO add your handling code here:
         p_login.setBackground(light_blue);
        lbl_login.setForeground(Color.white);
    }//GEN-LAST:event_lbl_loginMouseExited
    
    private void lbl_attachMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_attachMouseClicked
        // TODO add your handling code here:
    
        if(attach_flag == 0){
            ImageIcon i = new ImageIcon("E:/advancejava/NetBeansProject/lan-chat-messenger/chat_system/src/chat_system/x-button.PNG");
            lbl_attach.setIcon(i);
            ap = new attach_panel(ui);
            
            tf_msg.setVisible(false);
            ap.setVisible(true);
            ap.setBounds(0,0,670,304);
            lbl_send.setVisible(false);
            jPanel1.add(ap);
            
            attach_flag = 1;
        }
        
        else if(attach_flag == 1){
            ImageIcon i = new ImageIcon("E:/advancejava/NetBeansProject/lan-chat-messenger/chat_system/src/chat_system/attachment.PNG");
            lbl_attach.setIcon(i);
            ap.setVisible(false);
            tf_msg.setVisible(true);
            lbl_send.setVisible(true);
            
            attach_flag = 0;
        }
        jPanel1.updateUI();
    }//GEN-LAST:event_lbl_attachMouseClicked

    private void tf_msgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tf_msgActionPerformed
        // TODO add your handling code here:
        setMsg();
        add_msg(msg,"right");
        tf_msg.setText("");
    }//GEN-LAST:event_tf_msgActionPerformed
   void attachImageFile(){
       JFileChooser chooser=new JFileChooser();
       chooser.setFileFilter(new FileNameExtensionFilter("image","jpeg","jpg","png"));
       chooser.showOpenDialog(this);
       File source=chooser.getSelectedFile();
       if(source != null){
            sourceFile = source;
            add_file("right", sourceFile);
        }
   }
   void attachTextFile(){
       JFileChooser chooser=new JFileChooser();
       chooser.setFileFilter(new FileNameExtensionFilter("text","txt","docx","pdf"));
       chooser.showOpenDialog(this);
       File source=chooser.getSelectedFile();
       if(source != null){
            sourceFile = source;
            add_file("right", sourceFile);
        }
   }
    File getFile(){
        return sourceFile;
    }
    void clearFile(){
        sourceFile = null;
    }
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Chat_system.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Chat_system.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Chat_system.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Chat_system.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Chat_system().setVisible(true);
                new Chat_system().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JLabel lbl_attach;
    private javax.swing.JLabel lbl_current_name;
    private javax.swing.JLabel lbl_current_reciever;
    private javax.swing.JLabel lbl_login;
    private javax.swing.JLabel lbl_login_status;
    private javax.swing.JLabel lbl_name1;
    private javax.swing.JLabel lbl_name2;
    private javax.swing.JLabel lbl_name3;
    private javax.swing.JLabel lbl_send;
    private javax.swing.JPanel p_back;
    private javax.swing.JPanel p_contact1;
    private javax.swing.JPanel p_contact2;
    private javax.swing.JPanel p_contact3;
    private javax.swing.JPanel p_contacts;
    private javax.swing.JPanel p_left;
    private javax.swing.JPanel p_login;
    private javax.swing.JPanel p_login_back;
    private javax.swing.JPanel p_middle;
    private javax.swing.JPanel p_right;
    private javax.swing.JTextField tf_msg;
    private javax.swing.JTextField tf_password;
    private javax.swing.JTextField tf_search;
    // End of variables declaration//GEN-END:variables

    @Override
    public void mouseClicked(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mousePressed(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat_system;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.io.IOException;
import java.net.UnknownHostException;
import javax.swing.JOptionPane;
/**
 *
 * @author Yash
 */
public class MyClient {
    
    String input="", name;
    Chat_system ui;
    public MyClient(String name, Chat_system ui){
        System.out.println("Inside MyClient !!");
        this.name = name;
        this.ui = ui;
        
        try{
           // Chat_system sc = new Chat_system();
          client_call(); 
        }catch(Exception e){
          JOptionPane.showMessageDialog(null,"Exception in MyClient constructor "+e);  
        }
    }

    void client_call() throws UnknownHostException,IOException{
        int character;
        

        Socket s = new Socket("localhost",5000);

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        OutputStream clientOut = s.getOutputStream();
        try{
//                System.out.println("Enter your name !");
//                name = br.readLine();
                name = name + "\n";
                byte n_buff[] = name.getBytes();
                clientOut.write(n_buff);
               
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,"Exception in MyClient - client_call(): "+e);
        }

        C_Writer writer = new C_Writer(s,ui);

        C_Echoer c_echoer = new C_Echoer(s,ui);
    }
    
}

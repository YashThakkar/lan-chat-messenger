/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat_system;

import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;
import java.sql.SQLException;

/**
 *
 * @author HOME
 */
public class MysqlConnect {
    Connection conn;
    
    public static Connection connectDB(){
        try{
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/chat_system","yash","yash2700");
            JOptionPane.showMessageDialog(null,"Connection established successfully");
            return conn;
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null,"error while establishing connection ! "+e);
            return null;
        }
    } 
    
}

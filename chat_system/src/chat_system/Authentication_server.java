/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat_system;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author Yash
 */
public class Authentication_server extends Thread{

    Connection conn = null;
    ResultSet rs = null;
    PreparedStatement ps = null;
    String name;
    String password;
    boolean flag = false;
    public Authentication_server(){
        System.out.println("Inside Authentication server..");
        conn = MysqlConnect.connectDB();
        start();
    }
    public void run(){
        try{
            ServerSocket socket  = new ServerSocket(6000);
            while(true){
                Socket inSocket = socket.accept();
                BufferedReader in = new BufferedReader(new InputStreamReader(inSocket.getInputStream()));
                PrintWriter printt = new PrintWriter(inSocket.getOutputStream(),true);
                
                if(flag == false){
                    name = in.readLine();
                    password = in.readLine();
                    System.out.println("auth name: "+name);
                    flag = checkCredentials(name,password);
                }
                if(flag == true){
                    printt.println(flag);
                    flag = false;
                }else{
                    printt.println(flag);
                }   
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,"Error n Authentication server: "+e);
        }
    }
    boolean checkCredentials(String username, String pass){
        boolean check = false;
        try{
            String sql = "SELECT * FROM contacts";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                String n = rs.getString("name");
                String p = rs.getString("password");
                
                if((n.equals(username)) && (p.equals(pass))){
                    check = true;
                    break;
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,"Sql error in checkCredentials!!!: "+e);
        }
//        JOptionPane.showMessageDialog(null,"checkcred: "+check);
        return check;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        new Authentication_server();
    }
    
}

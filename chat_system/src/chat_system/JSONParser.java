/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat_system;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 *
 * @author Yash
 */
public class JSONParser {
    Chat_system ui;
    
    String result="";
    String msg = "";
    String datetime = "";
    String file = "";

    FileWriter out;
    FileReader in;// = new FileReader("mychats.json");
    
    JSONParser(Chat_system ui){
      this.ui = ui;  
    }
    
    public void writeJSON(String sendstr, String recstr, String msgstr, Date dtstr, File filestr){
        try {
            out = new FileWriter("mychats.json",true);
            StringBuilder format = new StringBuilder("{");
            format.append("\"sender\" : \""+sendstr+"\",");
            format.append("\"reciever\" : \""+recstr+"\",");
            format.append("\"msg\" : \""+msgstr+"\",");
            format.append("\"datetime\" : \""+dtstr+"\",");
            format.append("\"file\" : \""+filestr+"\"");
            format.append("}");
            String formatstr = format.toString();

            out.write(formatstr);
            out.flush();

            out.close();
        } catch (IOException ex) {
            Logger.getLogger(JSONParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void readJSON(String sender, String reciever, Chat_system csui) throws IOException{
        in = new FileReader("mychats.json");
        Chat_system uiobj = csui;
        String[] arr1= null;
        String[] arr2 = null;
        String htmlText = "";
            int n;
            while((n = in.read()) != -1){
                    char c = (char)n;
//                    System.out.println("c : "+c);
                    htmlText += c;
            }
            System.out.println("htmlText: "+htmlText);
//            JOptionPane.showMessageDialog(null,"htmlText: "+htmlText);
            
            String h2GroupPattern = "(\\{.*\\})";
            Pattern groupPattern = Pattern.compile(h2GroupPattern);
            Matcher groupMatcher = groupPattern.matcher(htmlText);
            System.out.println(groupMatcher.matches());
            groupMatcher.reset();
            while(groupMatcher.find()){
                    System.out.println("occurenece: "+groupMatcher.group());
                    result = groupMatcher.group();
            }
            int i = result.length();
            System.out.println("i- "+i);
            int index = 0;
            int send_flag = 0;
            int rec_flag = 0;
            int quotes = 0;
            String str = "";
            String curr_sender = "";
            String curr_reciever = "";
            String[] arr = new String[5];
            HashMap<String, String> keyvalue = new HashMap<String, String>();
            while(index<i){
                
                char ch = result.charAt(index);
                str = str + result.charAt(index);
                
                if(ch == ',' || ch == '}'){
                    
//                        JOptionPane.showMessageDialog(null,"str: "+str);
//                        arr = str.split(":");
                    /*not using above statement bcoz in file path it consist of colon so it was making prblm thats y developed some below 
                      logic to split the string by colon between key and value.
                    */
                    //splitting by colon algo
                    String tempo = "";
                    for(int k=0;k<str.length();k++){
                        char c = str.charAt(k);
                        tempo += c;
                        if(c == '"'){
                            quotes++;
                        }
                        if(c == ':'){
                            if(quotes%2 == 0){
//                                arr = str.split(":");
                                arr[0] = tempo;
                                tempo = "";
                                quotes = 0;
                            }
                            
                        }
                        if(c == '}' || c == ','){
                            arr[1] = tempo;
                            tempo = "";
                        }
                    }
                       
//                    JOptionPane.showMessageDialog(null,"arr[0] - "+arr[0] +" arr[1] - "+arr[1]);
                        
                    
                    String[] lhs = arr[0].split("\"");
                    
//                    JOptionPane.showMessageDialog(null,"lhs[0] - "+lhs[0] +" lhs[1] - "+lhs[1]);
                    
                    //below is used to remove comma
                    if(!((arr[1].trim()).equals("\"\","))){
                        arr1 = arr[1].split(",");
                        arr2 = arr1[0].split("\"");
//                        JOptionPane.showMessageDialog(null," arr2[0] - "+arr2[0] +" arr2[1] - "+arr2[1]);
                    }
                        
                    //putting into Hashmap
                    keyvalue.put(lhs[1], arr2[1]);
                    
//                        if(arr2[1].equals(sender)){
//                            JOptionPane.showMessageDialog(null,"sender: "+arr2[1]);
                            System.out.println("-----------------------");
//                            System.out.println("sender arr2[1]- "+arr2[1]);
                            curr_sender = keyvalue.get("sender");
                            send_flag = 1;
//                        }
//                        if(arr2[1].equals(reciever)){
//                            JOptionPane.showMessageDialog(null,"reciever: "+arr2[1]);
//                                System.out.println("reciever arr2[1]- "+arr2[1]);
                                curr_reciever = keyvalue.get("reciever");// arr2[1];
                                rec_flag = 1;
//                        }
                    //}
                    
                    // System.out.println("above  "+lhs[1]+" "+"msg");
                    // System.out.println("sf: "+send_flag+"   rf: "+rec_flag);
                    // System.out.println("bool: "+lhs[1].equals("msg"));
//                    JOptionPane.showMessageDialog(null,"sf: "+send_flag+" rf: "+rec_flag);
                    if(send_flag == 1 && rec_flag == 1){
//                        JOptionPane.showMessageDialog(null,"INSIDE");
                        if(lhs[1].equals("msg") && (!((arr[1].trim()).equals("\"\",")))){
                                msg = arr2[1];
//                                JOptionPane.showMessageDialog(null,"(msg) arr2[1] : "+arr2[1]);
                        }
                        if(lhs[1].equals("datetime")){
                                datetime = arr2[1];
                                // System.out.println("datetime- "+datetime);
                        }
                        if(lhs[1].equals("file")){
                                file = arr2[1];
//                                JOptionPane.showMessageDialog(null,"File : "+file);
                        }
//                        JOptionPane.showMessageDialog(null,"arr2[0] : "+arr2[0] + "arr2[1]: "+arr2[1]);
                    }
                    str = "";
                }if(ch == '}'){
                    send_flag = 0;
                    rec_flag = 0;

                    System.out.println("msg = "+msg);
                    System.out.println("datetime = "+datetime);
                    System.out.println("file = "+file);
                    System.out.println("-----------------------");
//                    JOptionPane.showMessageDialog(null,"msg: "+msg);
//                    if(!(msg.equals("\"\","))){
                    if(!(msg.equals("\"\"")|| msg.equals(""))){
                        String current_name = ui.get_current_name();
//                        JOptionPane.showMessageDialog(null,"(msg)current_name: "+current_name+" curr_sender: "+curr_sender);
                        if((current_name.equals(curr_sender)))
                            uiobj.add_msg(msg,"right");
                        else
                            uiobj.add_msg(msg,"left");
                        
                        msg = "";
                    }
                    else if(!(file.equals(""))){
                        File f = new File(file);
                        String current_name = ui.get_current_name();
//                        JOptionPane.showMessageDialog(null,"file------(file)current_name: "+current_name+" curr_sender: "+curr_sender);
                        if(current_name.equals(curr_sender))
                            uiobj.add_file("right",f);
                        else
                            uiobj.add_file("left",f);  
                        
                        file = "";
                    }
                }
            // i--;
            index++;
        }
    }
}
